<?php
    class Request{
        private $conn;
        private $db_table = "insurance";
        private $json_file = "../requests.json";

        public $cardid;
        public $firstname;
        public $lastname;
        public $age;
        public $salart;
        public $details;

        // Db connection
        public function __construct($db=null){
            $this->conn = $db;
        }

        private function saveLog($action, $obj) {
            $details = 'firstName: ' . $obj['firstname'] . ', lastName: ' . $obj['lastname'] . ', cardid: ' . $obj['cardid'] . ', age: ' . $obj['age'] . ', salart: ' . $obj['salart'] . ', details: ' . $obj['details'] . '.';
            $sqlQuery = "INSERT INTO
                        logs
                    SET
                        `action` = :ac, 
                        details = :details, 
                        `date` = NOW()
                        ";
        
            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(":ac", $action);
            $stmt->bindParam(":details", $details);

            
            if($stmt->execute()){
                return true;
             }             
            return false;
        }

        //Get all from json file
        public function getRequests($sortby='id', $searchby='id', $search=null){
            $inp = file_get_contents($this->json_file);
            $tempArray = json_decode($inp, true);

            //Search
            if($searchby && $search) {
                $arr = [];
                
                foreach($tempArray as $obj) {
                    if (stripos($search, ((string) ($obj[$searchby]))) !== false){
                        array_push($arr, $obj);
                    }
                }
            }else {
                $arr = $tempArray;
            }

            //Sort
            usort($arr, function($a, $b) use ($sortby) { return strcmp($a[$sortby], $b[$sortby]); });
            
            if(!$arr) $arr = [];

            return $arr;
        }

        // CREATE and push to json file
        public function createRequest(){
            $tempArray = $this->getRequests();

            if(count($tempArray) > 1) {
                $id = end($tempArray)['id'] + 1;
            }else {
                $id = 1;
            }
           
            $arr = array("cardid" => $this->cardid, "firstname" => $this->firstname, "lastname" => $this->lastname, "age" => $this->age, "salart" => $this->salart, "details" => $this->details, "id" => $id);

            array_push($tempArray, $arr);
            $jsonData = json_encode($tempArray);
            file_put_contents($this->json_file, $jsonData);

            return true;
        }

        // Remove from json file and push to db
        public function clearRequest($id){
           
            $val = $this->deleteRequest($id, true);

            $sqlQuery = "INSERT INTO
                        ". $this->db_table . "
                    SET
                        cardid = :cardid, 
                        firstname = :firstname, 
                        lastname = :lastname, 
                        age = :age, 
                        salart = :salart,
                        details = :details";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $val['cardid'] = htmlspecialchars(strip_tags($val['cardid']));
            $val['firstname'] = htmlspecialchars(strip_tags($val['firstname']));
            $val['lastname'] = htmlspecialchars(strip_tags($val['lastname']));
            $val['age'] = htmlspecialchars(strip_tags($val['age']));
            $val['salart'] = htmlspecialchars(strip_tags($val['salart']));
            $val['details'] = htmlspecialchars(strip_tags($val['details']));
        
            $stmt->bindParam(":cardid", $val['cardid']);
            $stmt->bindParam(":firstname", $val['firstname']);
            $stmt->bindParam(":lastname", $val['lastname']);
            $stmt->bindParam(":age", $val['age']);
            $stmt->bindParam(":salart", $val['salart']);
            $stmt->bindParam(":details", $val['details']);

            if($stmt->execute()){
               return true;
            }
            
            return false;

        }        

        private function moveElement(&$array, $a, $b) {
            $out = array_splice($array, $a, 1);
            array_splice($array, $b, 0, $out);
        }

        // UPDATE
        public function updatePosition($id){

            $arr = $this->getRequests();
            $index = 0;

            foreach($arr as $obj) {
                if ($obj['id'] == $id) {
                    $this->saveLog('Position Changed', $obj);
                    break;
                }
                $index += 1;
            }

            if($index > 0) {
                $this->moveElement($arr, $index, ($index - 1));
                file_put_contents($this->json_file, json_encode($arr));
                return true;
            } else {
                return false;
            }
        }

        // DELETE
        public function deleteRequest($id, $save=false){
            $arr = $this->getRequests();

            foreach($arr as $key => $obj) {
                if ($obj['id'] == $id) {
                    $tmp = $arr[$key];
                    
                    if($save) {
                        $this->saveLog('Approved', $obj);
                    }else {
                        $this->saveLog('Deleted', $obj);
                    }

                    unset($arr[$key]);
                    file_put_contents($this->json_file, json_encode($arr));
                    return $tmp;
                }
            }

            return null;
        }

        
    }
?>