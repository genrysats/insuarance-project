<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Request.php';

    $data = json_decode(file_get_contents("php://input"));
    
    if(!isset($data->cardid) || !isset($data->firstname) || !isset($data->lastname) || !isset($data->age) || !isset($data->salart) || !isset($data->details)) {
        echo json_encode(['success' => false, 'message' => 'Missing params']); exit;
    }


    $database = new Database();
    $db = $database->getConnection();

    $item = new Request($db);
    
    $item->cardid = $data->cardid;
    $item->firstname = $data->firstname;
    $item->lastname = $data->lastname;
    $item->age = $data->age;
    $item->salart = $data->salart;
    $item->details = $data->details;

    if($item->createRequest()){
        echo json_encode(['success' => true, 'message' => 'Request has been added successfully']);
    } else{
        echo json_encode(['success' => false, 'message' => 'Error occurred, please try again later']);
    }
?>