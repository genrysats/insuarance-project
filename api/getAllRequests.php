<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Request.php';

    if(!isset($_GET["page"])) {
        echo json_encode(['success' => false, 'message' => 'Missing page']); exit;
    }

    $sort = 'cardid';
    $searchby = '';
    $search = null;

    if(isset($_GET['sortby'])) {
        $sort = strtolower($_GET['sortby']);
    }
    if(isset($_GET['searchby'])) {
        $searchby = strtolower($_GET['searchby']);
    }
    if(isset($_GET['search'])) {
        $search = strtolower($_GET['search']);
    }

    if($sort == 'salary') {
        $sort = 'salart';
    }

    if($sort == 'id') $sort = 'cardid';
    if($searchby == 'id') $searchby = 'cardid'; 

    $page = $_GET["page"];

    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Request($db);

    $limit = ($page - 1) * 10;
    $items = $item->getRequests($sort, $searchby, $search);
    
    echo json_encode(['success' => true, 'arr' => array_slice($items,$limit,$limit + 10), 'pages' => ceil(count($items) / 10) ]);
?>