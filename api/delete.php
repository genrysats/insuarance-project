<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Request.php';

    if(!isset($_GET["id"])) {
        echo json_encode(['success' => false, 'message' => 'Missing id']); exit;
    }

    $saveDB = 0;

    if(isset($_GET["save"])) {
        $saveDB = $_GET['save'];
    }

    $id = $_GET["id"];

    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Request($db);

    if(($saveDB && $item->clearRequest($id)) || (!$saveDB && $item->deleteRequest($id))) {
        echo json_encode(['success' => true, 'message' => 'Deleted successfully' ]);
    }else {
        echo json_encode(['success' => false, 'message' => "Couldn't delete this request" ]);
    }
?>