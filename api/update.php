<?php 
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/Database.php';
    include_once '../class/Request.php';

    if(!isset($_GET["id"])) {
        echo json_encode(['success' => false, 'message' => 'Missing id']); exit;
    }

    $id = $_GET["id"];

    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Request($db);

    if($item->updatePosition($id)) {
        echo json_encode(['success' => true, 'message' => 'Updated successfully' ]);
    }else {
        echo json_encode(['success' => false, 'message' => "Couldn't update this request" ]);
    }
?>